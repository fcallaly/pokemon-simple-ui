# Frontend Exercise

Create a UI that fetches data from your Lol REST service.

Remember to:

1. Initially keep it as simple as possible - for now, focus on just retrieving the data from the Java program.

2. Add CrossOrigin("\*") to your Java RestController class.

3. If you finish, you may consider going further by adding a POST, to send data.
An example of this for the Pokemon service will be found on a branch in this repository. Ask your instructor when you're ready!
